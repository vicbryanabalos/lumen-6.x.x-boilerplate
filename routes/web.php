<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/** @var $router $router */
//$router->get('/', function () use ($router) {
//    return $router->app->version();
//});



$router->group(['prefix' => 'news'], function () use ($router) {

//getall
    $router->get('', 'NewsController@index');
    $router->get('{id:[0-9]*}', 'NewsController@get');
    $router->post('', 'NewsController@store');
    $router->delete('/{id:[0-9]*}', 'NewsController@destroy');

});

$router->group(['prefix' => 'comment'], function () use ($router) {

    $router->post('/{id:[0-9]*}', 'CommentsController@store');
    $router->delete('/{id:[0-9]*}', 'CommentsController@destroy');

});
