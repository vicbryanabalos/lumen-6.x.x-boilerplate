<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->get('/');
        $this->assertEquals(
            $this->app->version(), $this->response->getContent()
        );
    }

    public function testExample2()
    {
        $this->post('/news');
        print_r($this->response->getContent());
        $this->assertEquals(
            "get users", $this->response->getContent()
        );
    }

    public function testExample3()
    {
        $this->get('/news');
        $this->assertEquals(
            "get users", $this->response->getContent()
        );
    }

}
