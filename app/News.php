<?php

namespace App;

use Illuminate\Support\Facades\DB;

class News
{

    public static function getAll()
    {
        $news = DB::select('select * from news');

        foreach ($news as $key => $value) {
            $news[$key]->comments = DB::select('select * from comment where news_id = ?', [$value->id]);
        }

        return count($news) > 0 ? $news : false;
    }

    public static function get($id)
    {
        $news = DB::select('select * from news where id = ?', [$id]);
        foreach ($news as $key => $value) {
            $news[$key]->comments = DB::select('select * from comment where news_id = ? order by created_at asc', [$value->id]);
        }
        return count($news) > 0 ? $news : false;
    }

    public static function insert($request)
    {

        $id = DB::table('news')->insertGetId(
            [
                'title' => $request->get('title'),
                'body' => $request->get('body'),
                'created_at' => new \DateTime()
            ]
        );

        return $id;
    }

    public static function destroy($id)
    {
        $news = DB::table('news')->where('id', '=', $id);

        if ($news) {
            DB::table('comment')->where('news_id', '=', $id)->delete();
        }

        return $news ? $news->delete() : false;;
    }
}
