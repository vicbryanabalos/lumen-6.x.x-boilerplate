<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Auth\Authorizable;

class Comments
{
    public static function insert($id,$request)
    {

        $id = DB::table('comment')->insertGetId(
            [
                'body' => $request->get('body'),
                'news_id' => $id,
                'created_at' => new \DateTime()
            ]
        );


        return $id;
    }

    public static function destroy($id)
    {
        $news = DB::table('comment')->where('id', '=', $id);
        return $news ? $news->delete() : false;;
    }
}
