<?php

namespace App\Http\Controllers;

use App\Comments;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    public function store($id,Request $request)
    {

        $this->validate($request, [
            'body' => 'required'
        ]);

        Comments::insert($id,$request);

        return redirect('/news/' . $id);
    }

    public function destroy($id)
    {
        $return = Comments::destroy($id);
        return $return ? [
            'status_code' => 200,
            'message' => 'Comment deleted : ' . $id,
        ] : [
            'status_code' => 204,
            'message' => 'no content',
        ];
    }
}
