<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function index()
    {
        $news = News::getAll() ?? false;

        return $news ? [
            'status_code' => 200,
            'data' => $news,
        ] : [
            'status_code' => 204,
            'message' => 'no content',
        ];
    }

    public function get($id)
    {
        $news = News::get($id);
        return $news ? [
            'status_code' => 200,
            'data' => $news,
        ] : [
            'status_code' => 204,
            'message' => 'no content',
        ];
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'title' => 'required',
            'body' => 'required'
        ]);

        $id = News::insert($request);

        return redirect('/news/' . $id);
    }

    public function destroy($id)
    {
        $return = News::destroy($id);
        return $return ? [
            'status_code' => 200,
            'message' => 'News deleted : ' . $id,
        ] : [
            'status_code' => 204,
            'message' => 'no content',
        ];
    }
}
